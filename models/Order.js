// Order
//  - totalAmount (number)
//  - purchasedOn (Date - defaults to current date of creation)
//  - Must be associated with:
//    * A user who owns the order
//    * Products that belong to the order

// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose')

// [SECTION] Blueprint Schema
	const orderSchema = new mongoose.Schema({
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		purchasedBy: {
			userId: {
				type: String,
				required: [true, 'User ID is required']
			}
		},
		items: [
			{
				productId: {
					type: String,
					required: [true, 'Product ID is required']
				},
				quantity: {
					type: Number,
					required: [true, 'No. of item is required']
				}
			}
		]
	})

// [SECTION] Model
	const Order = mongoose.model('Order', orderSchema)
	module.exports = Order