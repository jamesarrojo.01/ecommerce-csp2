// [SECTION] Packages and Dependencies
	const express = require("express")
	const mongoose = require("mongoose")
	const dotenv = require("dotenv")

// [SECTION] Server Setup
	const myApp = express()
	dotenv.config()
	const secret = process.env.CONNECTION_STRING
	const port = process.env.PORT

// [SECTION] Database Connection
	mongoose.connect(secret)
	let connectionStatus = mongoose.connection
	connectionStatus.on('open', () => console.log('Database is connected'))

// [SECTION] Gateway Response
	myApp.get('/', (req, res) => {
		res.send(`eCommerce app is under construction`)
	})
	myApp.listen(port, () => console.log(`Server is running on port ${port}`))